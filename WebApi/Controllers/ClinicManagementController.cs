﻿namespace WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Web.Http;
    using Twilio;
    using WebApi.Models;
    using System.Linq;
    using System.Threading;
    public class ClinicManagementController : ApiController
    {
        readonly Models.DAL.DataAccessRepo dalObj = Models.DAL.DataAccessRepo.Instance;

        [ActionName("GetAppointmentByID")]
        public List<Message> GetAppointment(int id)
        {
            try
            {
                List<Message> responseMsg = new List<Message>();

                Customer customerDetails = null;
                string message;

                if (dalObj.IsValidCustomer(id))
                {
                    customerDetails = dalObj.GetCustomerByCustomerId(id);
                }
                else
                {
                    return new List<Message> { new Message { ErrorMessage = ConfigurationManager.AppSettings[Common.Helper.no_Customer_Message].ToString() } };
                }


                List<Models.Appointment> appointments = GetSlots(id, DateTime.Now.Date).ToList();


                if (!String.IsNullOrWhiteSpace(appointments.FirstOrDefault().DepartmentName))
                {
                    List<Appointment> appointmentsRedayForPayment = appointments.Where(x => x.IsCompleted == Convert.ToInt16(true)).ToList();
                    List<Appointment> appointmentsRedayForConsultation = appointments.Where(x => x.IsCompleted == Convert.ToInt16(false)).ToList();
                    if (appointmentsRedayForConsultation.Count > 0)
                    {
                        Appointment app = appointmentsRedayForConsultation.FirstOrDefault();
                        message = "Thank you for using EzClinic.This is an acknowledgement for your today's upcoming appointment with Dr. " + app.StaffFirstName + ", " + app.DepartmentName + "  on  " + Common.Helper.ReturnTime(app.SlotId.ToUpperInvariant()) + ".Directions with in EzClinic to reach consultation room No : " + app.RoomNo + " - From Main entrance of floor no " + app.Floor + " Follow  " + app.Direction + ".";
                        responseMsg.Add(Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message));
                    }
                    else
                    {
                        message = appointments.FirstOrDefault().Message;
                        responseMsg.Add(Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message));
                    }

                    ///Commented out since we have written a separate method to do this task (From Doctor's NFC Reader)
                    //if (appointmentsRedayForPayment.Count > 0)
                    //{
                    //   message = "Thank you for using EzClinic. Here is your payment URL for completing the Payment. " + ConfigurationManager.AppSettings["Payment_URL"] + "/" + id;
                    //    responseMsg.Add(Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message));
                    //}

                }

                else
                {
                    message = appointments.FirstOrDefault().Message;
                    responseMsg.Add(Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message));
                }

                return responseMsg;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
            finally
            {
                Logger.Info("");
            }
        }

        [HttpGet]
        [ActionName("GetSlots")]
        public IEnumerable<Appointment> GetSlots(int customerId, DateTime dt, bool? isReadyForPayment = null, bool? isReadyForConsultation = null)
        {
            try
            {
                List<Models.Appointment> appointments = dalObj.GetAppointmentsByCustomerId(customerId, dt);

                if (isReadyForPayment == true)
                {
                    return appointments.Where(x => x.IsCompleted == Convert.ToInt16(true));
                }
                else if (isReadyForConsultation == true)
                {
                    return appointments.Where(x => x.IsCompleted == Convert.ToInt16(false));
                }
                else
                {
                    return appointments;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
            finally
            {
                Logger.Info("");
            }

        }

        [HttpGet]
        [ActionName("GetStaffByDepartmentId")]
        public IEnumerable<Staff> GetStaffByDepartmentId(int id)
        {
            var deptObj = dalObj.GetStaffByDepartmentId(id);
            return (deptObj);

        }

        [HttpGet]
        [ActionName("GetTimeSlots")]
        public IEnumerable<AppSlot> GetTimeSlots()
        {
            var slotObj = dalObj.GetAllTimeSlots();
            return (slotObj);

        }

        [HttpGet]
        [ActionName("GetAppointmentsByDeptId")]
        public IEnumerable<Appointment> GetStaffByDepartmentIdAndDate(DateTime ts, int deptid)
        {
            return dalObj.GetStaffByDepartmentIdAndDate(ts, deptid);
        }

        [HttpGet]
        [ActionName("GetCustomer")]
        public Models.Customer GetCustomer(int id)
        {
            try
            {
                return dalObj.GetCustomerByCustomerId(id);


            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }

        }

        [HttpPost]
        [ActionName("AddAppointment")]
        public int BookAppointment(Appointment appObj)
        {
            int currentAppointmentId = dalObj.BookAppointment(appObj);
            if (currentAppointmentId > 0)
            {
                Customer customerDetails = dalObj.GetCustomerByCustomerId(appObj.CustomerId);
                Appointment app = GetSlots(appObj.CustomerId, appObj.AppointmentDate, null, true).Where(x => x.AppointmentId == currentAppointmentId).FirstOrDefault();
                string message = "Thank you for using EzClinic.Appointment confirmation for  " + customerDetails.FirstName + " with Dr." + app.StaffFirstName + ", " + app.DepartmentName + ". On " + app.AppointmentDate.ToShortDateString() + "  at  " + Common.Helper.ReturnTime(app.SlotId.ToUpper()) + ".";
                Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message);
                return currentAppointmentId;
            }
            return 0;
        }

        [ActionName("GetDepartments")]
        public IEnumerable<Department> GetDepartments()
        {
            var deptLst = dalObj.GetAllDepartments();
            return deptLst;
        }

        // POST: api/ClinicManagement
        [HttpPost]
        [ActionName("RegisterCustomer")]
        public Models.Customer RegisterCustomer(Models.Customer customerObj)
        {
            try
            {
                if (customerObj != null)
                {
                    int custId = dalObj.RegisterCustomer(customerObj);
                    return dalObj.GetCustomerByCustomerId(custId);
                }
                else
                {
                    return new Models.Customer { Message = Common.Helper.no_Customer_Message };
                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return new Models.Customer { Message = ex.Message };
            }
        }

        [HttpGet]
        [ActionName("IsValidCustomer")]
        public bool IsValidCustomer(int id)
        {
            return dalObj.IsValidCustomer(id);
        }

        [HttpGet]
        [ActionName("CompleteAppointment")]
        public bool CompleteAppointment(int id)
        {
            return dalObj.CompleteConsultation(id);
        }

        [HttpGet]
        [ActionName("CompleteAppointmentByDoctor")]
        public List<Message> CompleteAppointmentByDoctor(int id)
        {
            try
            {
                List<Message> responseMsg = new List<Message>();

                Customer customerDetails = null;
                string message;

                if (dalObj.IsValidCustomer(id))
                {
                    customerDetails = dalObj.GetCustomerByCustomerId(id);
                }
                else
                {
                    return new List<Message> { new Message { ErrorMessage = ConfigurationManager.AppSettings[Common.Helper.no_Customer_Message].ToString() } };
                }


                List<Models.Appointment> appointments = GetSlots(id, DateTime.Now.Date).ToList();


                if (!String.IsNullOrWhiteSpace(appointments.FirstOrDefault().DepartmentName))
                {
                    List<Appointment> appointmentsRedayForConsultation = appointments.Where(x => x.IsCompleted == Convert.ToInt16(false)).ToList();
                    if (appointmentsRedayForConsultation.Count > 0)
                    {
                        Appointment app = appointmentsRedayForConsultation.FirstOrDefault();
                        Logger.Info("Appointment ID: " + app.AppointmentId.ToString().Trim());
                        CompleteAppointment(Int16.Parse(app.AppointmentId.ToString().Trim()));
                        message = "Thank you for using EzClinic. Tap your NFC on the Payment reader or use this payment URL for completing the Payment. " + ConfigurationManager.AppSettings["Payment_URL"] + "/" + id;
                        responseMsg.Add(Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message));
                    }
                    else
                    {
                        message = appointments.FirstOrDefault().Message;
                        responseMsg.Add(Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message));
                    }
                }

                else
                {
                    message = appointments.FirstOrDefault().Message;
                    responseMsg.Add(Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message));
                }

                return responseMsg;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
            finally
            {
                Logger.Info("");
            }
        }

        [HttpPost]
        [ActionName("AddTransaction")]
        public string AddTransaction(int id, bool isPaid)
        {
            if (isPaid)
            {
                IEnumerable<Appointment> allSlotsReadyForPayment = GetSlots(id, DateTime.Now, true);
                double consultationFeeToBePaid = 0.00;
                string transactionRef = dalObj.AddTransaction(allSlotsReadyForPayment.Select(x => new Transaction { TransactionAmt = x.ConsultationFee, AppointmentId = x.AppointmentId, CustomerId = id }));

                foreach (Appointment item in allSlotsReadyForPayment)
                {
                    consultationFeeToBePaid = consultationFeeToBePaid + item.ConsultationFee;
                }

                Customer customerDetails = dalObj.GetCustomerByCustomerId(id);
                string message;
                int i = 1;
                foreach (Appointment app in allSlotsReadyForPayment)
                {
                    if (i >= 2)
                        Thread.Sleep(1000);
                    message = "Thank you for using EzClinic. Your payment for " + app.DepartmentName + " consultation " + i + " with Dr. " + app.StaffFirstName + " is scussessful." + "Consultation charge for " + app.DepartmentName + " is " + app.ConsultationFee + " out of your total payment " + consultationFeeToBePaid.ToString() + ". Ref Id : " + transactionRef + ".";
                    Common.Helper.SendSMS(Common.Helper.countryExtn + customerDetails.MobileNo.ToString(), message);
                    i++;
                }

                return transactionRef;
            }
            return "No Payment Dues";
        }
    }
}

