﻿namespace WebApi.Common
{
    using System.Configuration;

    public static class Helper
    {
        public static readonly string no_Customer_Message = ConfigurationManager.AppSettings["NO_PATIENT_MESSAGE"];
        public static readonly string accountSID = ConfigurationManager.AppSettings["AccountSID"];
        public static readonly string authToken = ConfigurationManager.AppSettings["AuthToken"];
        public static readonly string mobileFrom = ConfigurationManager.AppSettings["MobileFrom"];
        public static readonly string countryExtn = ConfigurationManager.AppSettings["IndiaPhoneExtn"];
        public static readonly string failureMessage = ConfigurationManager.AppSettings["FailureMessage"];
        public static readonly string noAppointmentMessage = ConfigurationManager.AppSettings["NoAppointmentMessage"];

        public static Twilio.Message SendSMS( string mobileNo, string message)
        {
            var twilio = new Twilio.TwilioRestClient(Common.Helper.accountSID, Common.Helper.authToken);
            Twilio.Message twilioResult = twilio.SendMessage(Common.Helper.mobileFrom, mobileNo, message, "");
            return twilioResult;
        }

        public static string ReturnTime(string slotId)
        {
            switch (slotId)
            {
                case "S01": return "10:00";
                case "S02": return "10:30";
                case "S03": return "11:00";
                case "S04": return "11:30";
                case "S05": return "12:00";
                case "S06": return "14:00";
                case "S07": return "14:30";
                case "S08": return "15:00";
                case "S09": return "15:30";
                case "S10": return "16:00";

                default:
                    return string.Empty;
            }
        }
    }
}