﻿// ======================================================
// <copyright file="Customer.cs" company="CSC">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>Gopi Katari</author>
// <date>03/17/2016 10:00:00 AM </date>
// <summary>Class representing a Customer entity</summary>
//======================================================
namespace WebApi.Models
{
    using System;

    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public Int64 MobileNo { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public string CardCategeory { get; set; }
        public string CreditCardType { get; set; }
        public Int64 CreditCardNo { get; set; }
        public int CreditCardExpMonth { get; set; }
        public int CreditCardExpYear { get; set; }
        public string NameOnCard { get; set; }
    }
}