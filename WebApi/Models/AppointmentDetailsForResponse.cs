﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class AppointmentDetailsForResponse
    {
        public int AppointmentId { get; set; }
        public string SlotId { set; get; }
        public int IsCompleted { set; get; }
        public string StaffFirstName { get; set; }
        public int Floor { get; set; }
        public int RoomNo { get; set; }
        public string Direction { get; set; }
        public string Message { get; set; }
        public string SlotTime { set; get; }
        public string DepartmentName { set; get; }
    }
}