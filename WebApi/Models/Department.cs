﻿// ======================================================
// <copyright file="Department.cs" company="CSC">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>Gopi Katari</author>
// <date>03/15/2016 10:00:00 AM </date>
// <summary>Class representing a Department entity</summary>
//======================================================
namespace WebApi.Models
{
    public class Department
    {
        public int DeptId { get; set; }
        public string DeptName { set; get; }
        public int selected { get; set; }
    }
}