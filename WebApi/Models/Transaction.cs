﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Transaction
    {
        public int TransId { set; get; }
        public string  TransReference { get; set; }
        public DateTime TransactionDate { set; get; }
        public double TransactionAmt { set; get; }
        public int AppointmentId { set; get; }
        public int CustomerId { set; get; }
        public int IsPaid { set; get; }
    }
}