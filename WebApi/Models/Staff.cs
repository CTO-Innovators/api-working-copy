﻿namespace WebApi.Models
{
    using System.Collections.Generic;
    using System;
    public class Staff
    {
        public int StaffID { get; set; }
        public string StaffFirstName { get; set; }
        public string StaffLastName { get; set; }
        public int DeptId { get; set; }
        public double ConsultaionFee { get; set; }
        public int StaffExtension { get; set; }
        public int Floor { get; set; }
        public int RoomNo { get; set; }
        public string Direction { get; set; }
    }
}