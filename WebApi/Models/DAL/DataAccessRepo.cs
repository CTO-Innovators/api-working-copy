﻿namespace WebApi.Models.DAL
{
    using Npgsql;
    using NpgsqlTypes;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    public sealed class DataAccessRepo
    {
        private DataAccessRepo()
        {
        }

        public static DataAccessRepo Instance { get { return Nested.instance; } }

        static private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly DataAccessRepo instance = new DataAccessRepo();
        }

        public int RegisterCustomer(Customer customerObj)
        {
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  RegisterCustomer ...." + DateTime.Now);
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    // Open the PgSQL Connection.                  
                    pgsqlConnection.Open();

                    string insertCommand = "spInsertCustomer";

                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(insertCommand, pgsqlConnection))
                    {
                        using (NpgsqlTransaction tran = pgsqlConnection.BeginTransaction())
                        {

                            pgsqlcommand.CommandType = CommandType.StoredProcedure;

                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("firstName", NpgsqlDbType.Varchar));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("lastName", NpgsqlDbType.Varchar));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("date_of_birth", NpgsqlDbType.Date));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("gender", NpgsqlDbType.Char));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("address", NpgsqlDbType.Varchar));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("mobile_no", NpgsqlDbType.Bigint));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("email_id", NpgsqlDbType.Varchar));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("card_category", NpgsqlDbType.Varchar));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("card_type", NpgsqlDbType.Varchar));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("card_no", NpgsqlDbType.Bigint));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("card_expiry_month", NpgsqlDbType.Integer));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("card_expiry_year", NpgsqlDbType.Integer));
                            pgsqlcommand.Parameters.Add(new NpgsqlParameter("name_on_the_card", NpgsqlDbType.Varchar));

                            pgsqlcommand.Parameters[0].Value = customerObj.FirstName;
                            pgsqlcommand.Parameters[1].Value = customerObj.LastName;
                            pgsqlcommand.Parameters[2].Value = customerObj.DateOfBirth;
                            pgsqlcommand.Parameters[3].Value = customerObj.Gender;
                            pgsqlcommand.Parameters[4].Value = customerObj.Address;
                            pgsqlcommand.Parameters[5].Value = customerObj.MobileNo;
                            pgsqlcommand.Parameters[6].Value = customerObj.Email;
                            pgsqlcommand.Parameters[7].Value = customerObj.CardCategeory;
                            pgsqlcommand.Parameters[8].Value = customerObj.CreditCardType;
                            pgsqlcommand.Parameters[9].Value = customerObj.CreditCardNo;
                            pgsqlcommand.Parameters[10].Value = customerObj.CreditCardExpMonth;
                            pgsqlcommand.Parameters[11].Value = customerObj.CreditCardExpYear;
                            pgsqlcommand.Parameters[12].Value = customerObj.NameOnCard;

                            object res = pgsqlcommand.ExecuteScalar();
                            tran.Commit();

                            Logger.Info("Exiting from ClinicDBProvider -  RegisterCustomer .... " + DateTime.Now);
                            return Convert.ToInt32(res);
                        }
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  RegisterCustomer: " + ex.Message);
                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  RegisterCustomer: " + ex.Message);
                throw ex;
            }
        }

        public Customer GetCustomerByCustomerId(int customerId)
        {
            Customer result = null;
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  GetCustomerByCustomerId ...." + DateTime.Now);
                using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    using (NpgsqlCommand command = new NpgsqlCommand("spgetcustomerdetailsbycustid", connection))
                    {
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new NpgsqlParameter("customer_id", NpgsqlDbType.Integer));

                        command.Parameters[0].Value = customerId;

                        NpgsqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            result = new Customer
                            {
                                CustomerId = customerId,
                                Address = reader[0].ToString(),
                                CardCategeory = reader[1].ToString(),
                                CreditCardExpMonth = Convert.ToInt32(reader[2]),
                                CreditCardExpYear = Convert.ToInt32(reader[3]),
                                CreditCardNo = Convert.ToInt64(reader[4]),
                                CreditCardType = reader[5].ToString(),
                                DateOfBirth = Convert.ToDateTime(reader[6]),
                                Email = reader[7].ToString(),
                                FirstName = reader[8].ToString(),
                                Gender = reader[9].ToString(),
                                LastName = reader[10].ToString(),
                                MobileNo = Convert.ToInt64(reader[11]),
                                NameOnCard = reader[12].ToString()
                            };

                            connection.Close();
                        }
                        Logger.Info("Exiting from ClinicDBProvider -  GetCustomerByCustomerId ...." + DateTime.Now);
                        return result;
                    }
                }
            }

            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetCustomerByCustomerId : " + ex.Message);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetCustomerByCustomerId : " + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<Staff> GetStaffByDepartmentId(int deptId)
        {
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  spgetstaffdetailsbydeptid ...." + DateTime.Now);
                using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    using (NpgsqlCommand command = new NpgsqlCommand("spgetstaffdetailsbydeptid", connection))
                    {
                        connection.Open();

                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new NpgsqlParameter("deptId", NpgsqlDbType.Integer));
                        command.Parameters[0].Value = deptId;
                        NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        connection.Close();
                        Logger.Info("Exiting from ClinicDBProvider -  spgetstaffdetailsbydeptid ...." + DateTime.Now);
                        return dt.AsEnumerable().Select(row => new Staff { StaffID = Convert.ToInt16(row["staff_id"]), StaffFirstName = row["staff_first_name"].ToString(), StaffLastName = row["staff_last_name"].ToString(), ConsultaionFee = Convert.ToDouble(row["consultation_fee"]), StaffExtension = Convert.ToInt16(row["staff_extension"]), Floor = Convert.ToInt16(row["floor"]), RoomNo = Convert.ToInt16(row["room_no"]), Direction = row["direction"].ToString() });
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetStaffByDepartmentId : " + ex.Message);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetStaffByDepartmentId : " + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<AppSlot> GetAllTimeSlots()
        {
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  GetAllTimeSlots ...." + DateTime.Now);
                using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    using (NpgsqlCommand command = new NpgsqlCommand("spgetallslots", connection))
                    {
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        Logger.Info("Exiting from ClinicDBProvider -  GetAllTimeSlots ...." + DateTime.Now);
                        return dt.AsEnumerable().Select(row => new AppSlot { SlotID = row[0].ToString(), SlotTime = row[1].ToString() });
                    }
                }
            }

            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetAllTimeSlots : " + ex.Message);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetAllTimeSlots : " + ex.Message);
                throw ex;
            }

        }

        public IEnumerable<Appointment> GetStaffByDepartmentIdAndDate(DateTime date, int deptId)
        {
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  spgetstaffbydeptidanddate ...." + DateTime.Now);
                using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand("spgetstaffbydeptidanddate", connection))
                    {
                        connection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new NpgsqlParameter("deptId", NpgsqlDbType.Integer));
                        cmd.Parameters.Add(new NpgsqlParameter("date", NpgsqlDbType.Date));
                        cmd.Parameters[0].Value = deptId;
                        cmd.Parameters[1].Value = date.ToShortDateString();
                        NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        connection.Close();
                        Logger.Info("Exiting from ClinicDBProvider -  spgetstaffbydeptidanddate ...." + DateTime.Now);
                        return dt.AsEnumerable().Select(row => new Appointment { AppointmentDate = Convert.ToDateTime(row["appointment_date"]), AppointmentId = Convert.ToInt16(row["appointment_id"]), SlotTime = row["slot_time"].ToString(), SlotId = row["slot_id"].ToString(), IsCompleted = Convert.ToInt16(row["is_completed"]), StaffId = Convert.ToInt32(row["staff_id"]), StaffFirstName = row["staff_first_name"].ToString(), StaffLastName = row["staff_last_name"].ToString(), Floor = Convert.ToInt16(row["floor"]), RoomNo = Convert.ToInt16(row["room_no"]), Direction = row["direction"].ToString() });
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetStaffByDepartmentIdAndDate : " + ex.Message);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetStaffByDepartmentIdAndDate : " + ex.Message);
                throw ex;
            }
        }

        public int BookAppointment(Appointment appObj)
        {
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  BookAppointment ...." + DateTime.Now);
                using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    using (NpgsqlCommand command = new NpgsqlCommand("spinsertappointment", connection))
                    {
                        connection.Open();
                        using (NpgsqlTransaction tran = connection.BeginTransaction())
                        {

                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new NpgsqlParameter("appointment_date", NpgsqlDbType.Date));
                            command.Parameters.Add(new NpgsqlParameter("slot_id", NpgsqlDbType.Varchar));
                            command.Parameters.Add(new NpgsqlParameter("staff_id", NpgsqlDbType.Integer));
                            command.Parameters.Add(new NpgsqlParameter("customer_id", NpgsqlDbType.Integer));

                            command.Parameters[0].Value = appObj.AppointmentDate;
                            command.Parameters[1].Value = appObj.SlotId;
                            command.Parameters[2].Value = appObj.StaffId;
                            command.Parameters[3].Value = appObj.CustomerId;



                            object res = command.ExecuteScalar();
                            tran.Commit();
                            Logger.Info("Exiting from ClinicDBProvider -  BookAppointment ...." + DateTime.Now);
                            if (Convert.IsDBNull(res))
                                return 0;
                            return Convert.ToInt32(res);
                        }
                    }
                }
            }

            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  BookAppointment : " + ex.Message);
                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  BookAppointment : " + ex.Message);
                throw ex;
            }

        }

        public IEnumerable<Department> GetAllDepartments()

        {
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  GetAllDepartments ...." + DateTime.Now);
                using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    using (NpgsqlCommand command = new NpgsqlCommand("spgetalldepartments", connection))
                    {
                        connection.Open();

                        command.CommandType = CommandType.StoredProcedure;
                        NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        Logger.Info("Exiting from ClinicDBProvider -  GetAllDepartments ...." + DateTime.Now);
                        connection.Close();
                        return dt.AsEnumerable().Select(row => new Department { DeptId = Convert.ToInt16(row[0]), DeptName = row[1].ToString() });
                    }
                }

            }
            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetAllDepartments : " + ex.Message);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetAllDepartments : " + ex.Message);
                throw ex;
            }
        }

        public bool IsValidCustomer(int customerId)
        {
            try
            {
                Logger.Info("Entering from the IsValidCustomer " + DateTime.Now);
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    pgsqlConnection.Open();

                    string getCommand = "spgetisvalidcustomer";

                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(getCommand, pgsqlConnection))
                    {

                        pgsqlcommand.CommandType = CommandType.StoredProcedure;

                        pgsqlcommand.Parameters.Add(new NpgsqlParameter("customerid", NpgsqlDbType.Integer));

                        pgsqlcommand.Parameters[0].Value = customerId;

                        object res = pgsqlcommand.ExecuteScalar();

                        Logger.Info("Exiting from the spInsertCustomer " + DateTime.Now);

                        return Convert.ToBoolean(res);
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Appointment> GetAppointmentsByCustomerId(int customerId, DateTime appointmentDate)
        {
            try
            {
                Logger.Info("Entering into ClinicDBProvider -  spgetappointmentdetailsbycustid ...." + DateTime.Now);
                using (NpgsqlConnection connection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    using (NpgsqlCommand command = new NpgsqlCommand("spgetappointmentdetailsbycustid", connection))
                    {
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new NpgsqlParameter("customerid", NpgsqlDbType.Integer));
                        command.Parameters.Add(new NpgsqlParameter("appointmentDate", NpgsqlDbType.Date));

                        command.Parameters[0].Value = customerId;
                        command.Parameters[1].Value = appointmentDate.ToShortDateString();
                        NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        connection.Close();
                        Logger.Info("Exiting from ClinicDBProvider -  spgetappointmentdetailsbycustid ...." + DateTime.Now);
                        if (dt.Rows.Count > 0)
                            return dt.AsEnumerable().Select(row => new Appointment { AppointmentDate = appointmentDate.Date, AppointmentId = Convert.ToInt32(row[0]), IsCompleted = Convert.ToInt16(row[1]), SlotId = row[2].ToString(), StaffFirstName = row[3].ToString(), DepartmentName = row[4].ToString(), Floor = Convert.ToInt16(row[5]), RoomNo = Convert.ToInt16(row[6]), Direction = row[7].ToString(), ConsultationFee = Convert.ToDouble(row[8]), CustomerId = customerId }).ToList();
                        else
                            return new List<Appointment> { new Appointment { Message = ConfigurationManager.AppSettings["NoAppointmentMessage"] } };


                    }
                }
            }

            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetAppointmentByCustomerId : " + ex.Message);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  GetAppointmentByCustomerId : " + ex.Message);
                throw ex;
            }



        }


        public string AddTransaction(IEnumerable<Transaction> transObj)
        {
            try
            {
                var random = new Random();

                string transRef = "EZCTRXN" + random.Next(100000, 900000).ToString();

                object res = null;
                foreach (Transaction item in transObj)
                {

                    Logger.Info("Entering into ClinicDBProvider -  InsertTransation ...." + DateTime.Now);
                    using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                    {
                        // Open the PgSQL Connection.                  
                        pgsqlConnection.Open();

                        string insertCommand = "spinserttransaction";

                        using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(insertCommand, pgsqlConnection))
                        {
                            using (NpgsqlTransaction tran = pgsqlConnection.BeginTransaction())
                            {

                                pgsqlcommand.CommandType = CommandType.StoredProcedure;


                                pgsqlcommand.Parameters.Add(new NpgsqlParameter("trans_ref", NpgsqlDbType.Varchar));
                                pgsqlcommand.Parameters.Add(new NpgsqlParameter("transaction_amount", NpgsqlDbType.Double));
                                pgsqlcommand.Parameters.Add(new NpgsqlParameter("appointment_id", NpgsqlDbType.Integer));
                                pgsqlcommand.Parameters.Add(new NpgsqlParameter("customer_id", NpgsqlDbType.Integer));

                                pgsqlcommand.Parameters[0].Value = transRef;
                                pgsqlcommand.Parameters[1].Value = item.TransactionAmt;
                                pgsqlcommand.Parameters[2].Value = item.AppointmentId;
                                pgsqlcommand.Parameters[3].Value = item.CustomerId;

                                res = pgsqlcommand.ExecuteScalar();
                                tran.Commit();

                                Logger.Info("Exiting from ClinicDBProvider -  InsertTransation .... " + DateTime.Now);

                            }
                        }
                    }
                }

                return res.ToString();

            }
            catch (NpgsqlException ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  InsertTransation: " + ex.Message);
                throw ex;
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message from ClinicDBProvider -  InsertTransation: " + ex.Message);
                throw ex;
            }
        }

        public bool CompleteConsultation(int appointmentId)
        {
            try
            {
                Logger.Info("Entering from the CompleteConsultation " + DateTime.Now);
                using (NpgsqlConnection pgsqlConnection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ToString()))
                {
                    pgsqlConnection.Open();

                    string getCommand = "spudateappointmentcompletedflag";

                    using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(getCommand, pgsqlConnection))
                    {

                        pgsqlcommand.CommandType = CommandType.StoredProcedure;

                        pgsqlcommand.Parameters.Add(new NpgsqlParameter("appointmentid", NpgsqlDbType.Integer));

                        pgsqlcommand.Parameters[0].Value = appointmentId;

                        pgsqlcommand.ExecuteReader();

                        Logger.Info("Exiting from the CompleteConsultation " + DateTime.Now);

                        return true;
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}


