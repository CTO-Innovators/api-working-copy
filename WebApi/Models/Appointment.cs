﻿// ======================================================
// <copyright file="Appointment.cs" company="CSC">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>Gopi Katari</author>
// <date>03/15/2016 10:00:00 AM </date>
// <summary>Class representing a Appointment entity</summary>
//======================================================
namespace WebApi.Models
{
    using System;
    public class Appointment
    {
        public int AppointmentId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public string SlotId { set; get; }
        public int StaffId { get; set; }
        public int CustomerId { get; set; }
        public int IsCompleted { set; get; }        
        public string StaffFirstName { get; set; }
        public string StaffLastName { get; set; }
        public double ConsultationFee { get; set; }
        public int Floor { get; set; }
        public int RoomNo { get; set; }
        public string Direction { get; set; }
        public string Message { get; set; }
        public string SlotTime { set; get; }
        public string DepartmentName { set; get; }


    }
}